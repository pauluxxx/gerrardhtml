/*
 * Third party
 */
//= ../../bower_components/jquery/dist/jquery.min.js
//= ../../bower_components/magnific-popup/dist/jquery.magnific-popup.min.js
//= ../../bower_components/scrollme/jquery.scrollme.min.js
//= ../../bower_components/parallax.js/parallax.min.js
//= ../../bower_components/owl.carousel/dist/owl.carousel.min.js
//= ../../bower_components/waypoints/lib/noframework.waypoints.min.js
//= partials/bootstrap-formhelpers-phone.js
/*
 * Custom
 */
//= partials/main.js