$(function () {
//Chrome Smooth Scroll
    try {
        $.browserSelector();
        if ($("html").hasClass("chrome")) {
            $.smoothScroll();
        }
    } catch (err) {

    }
    ;

    $("img, a").on("dragstart", function (event) {
        event.preventDefault();
    });

    (function () {
        var lastTime = 0;
        var vendors = ['ms', 'moz', 'webkit', 'o'];
        for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
            window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame']
                || window[vendors[x] + 'CancelRequestAnimationFrame'];
        }

        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = function (callback, element) {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function () {
                        callback(currTime + timeToCall);
                    },
                    timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };

        if (!window.cancelAnimationFrame)
            window.cancelAnimationFrame = function (id) {
                clearTimeout(id);
            };
    }());
    var intitLink = $('#forms-colors .four a[href="#best1"]');
    var links = $('#forms-colors .four a');
    links.click(function (e) {
        intitLink.removeClass('active');
    })
    //parallax
    $('.parallax-window').parallax({
        imageSrc: 'img/backheader.png', zIndex: 0, iosFix: true, androidFix: true
    });
    //$('.parallax-slider').before('<div class="overlay"></div>');

    $(window).trigger('resize').trigger('scroll');
    //owl
    // BigCarousel = $('.basic.owl-carousel')
    // BigCarousel.owlCarousel()
    //
    var config_carousel = {
        loop: false,
        margin: 30,
        nav: false,
        animateOut: 'fadeOut',
        items: 1,
        mouseDrag: true,
        dots: false,
        touchDrag: false,
        startPosition: 'URLHash',
        URLhashListener: true,
        responsive: {
            0: {},
            768: {},
            1200: {}
        }
    }
    BigCarousel = $('#root_forms')
    BigCarousel.owlCarousel(config_carousel);
    SmallCarousel = $("#root_preview")
    SmallCarousel.owlCarousel(config_carousel);
    ColorsCarousel = $('.root_colors');
    ColorsCarousel.owlCarousel(config_carousel);

    BestGerrardCarousel = $('.worth')
    BestGerrardCarousel.owlCarousel({
            loop: true,
            margin: 20,
            nav: true,
            items: 1,
        animateOut: 'fadeOut',
            mouseDrag: true,
            dots: false,
        startPosition: 'URLHash',
        URLhashListener: true,
            touchDrag: false,
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            responsive: {
                0: {},
                768: {},
                1200: {}
            }
        }
    );
    CertificatesCarousel = $("#certificates");
    CertificatesCarousel.owlCarousel({
        loop: true,
        margin: 0,
        items: 3,
        nav: true,
        mouseDrag: true,
        dots: true,
        touchDrag: false,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        responsive: {
            0: {margin: 20},
            768: {margin: 40},
            1200: {margin: 0}
        }
    })
    $('#grid .small-form a').click(function (e) {
        $('#modal b').html($(this).text());
        $('#modal').attr("href", "#" + $(this).text());
    });
    $('#overlay').click(function (e) {
        $('#overlay').toggleClass('overlay')
        $('iframe').trigger('click');
    })
    $('.popup-with-zoom-anim').magnificPopup({
        type: 'inline',

        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
    });
    $('.popup-with-move-anim').magnificPopup({
        type: 'inline',

        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom'
    });
    $('.open-popup-link').magnificPopup({
        type: 'inline',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });
    $('.image-popup-no-margins').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 500 // don't foget to change the duration also in CSS
        }
    });
    if (window.screen.width < 960) {
        $('.scrollme').removeClass('animateme');
    }
    var ivan = 'ivan'
    var ivanCounter = 2;
    var nextIvan = ivan + ivanCounter;
    var currentIvan = ivan + 1;
    var ivanHrefContainer = '<div class=" col-12 ml-auto col-sm-9"><a href="" data-tab="" id="ivanHref">Читать больше отзывов</a></div>';
    $('#' + currentIvan).append(ivanHrefContainer);
    $('#ivanHref').attr("data-tab", "#" + nextIvan);
    $('#ivanHref').click(function runIvan(e) {
        e.preventDefault();
        $(this).parent().addClass('unvisible');
        setTimeout(function () {
            $('#certificate').addClass('moveDown');
        }, 0);
        setTimeout(function () {
            $('#ivanHref').remove()
        }, 450);
        $("#" + nextIvan);
        setTimeout(function () {
            $("#" + nextIvan).addClass('isVisible').removeClass('hide');
            $("#" + nextIvan + ' .overflow').addClass('isOut');
            ivanCounter++;
            nextIvan = ivan + ivanCounter;
            if ($('#' + nextIvan).size()) {
                $('#' + ivan + (ivanCounter - 1)).append(ivanHrefContainer);
                $('#ivanHref').attr("data-tab", "#" + nextIvan);
                $('#ivanHref').bind('click', runIvan)
            }
            $('#certificate').removeClass('moveDown');
        }, 500);
    })
    var headers = $('[data-randAninamte]');
    headers.each(function (i, header) {
        new Waypoint({
            element: header,
            handler: function (direction) {
                animateText($(this.element));
            },
            offset: "60%"
        })
        new Waypoint({
            element: header,
            handler: function (direction) {
                splitText($(this.element));
            },
            offset: "100%"
        })
    });
//     .viewportChecker({
//     callbackFunction: function (e, a) {
//         $(e).each(function (i, header) {
//
//         });
//     }
// });

    function splitText(context) {
        var contents = context.text(),
            letters = stringToArray(contents),
            markup = '';

        letters.forEach(function (letter) {
            markup += '<span>' + letter + '</span>';
        });

        context.html(markup);
    }

    function animateText(context) {
        var delay = 200;
        context.children().each(function (index, letter) {
            delay = getRandomArbitrary(200, 600);
            setTimeout(function () {
                $(letter).addClass('spanLoop');
            }, delay * (index / 15));
        });
    }

    function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }
})
;

function stringToArray(string) {
    var array = [],
        length = string.length,
        index = 0;

    for (; index < length; ++index) {
        array.push(string[index]);
    }

    return array;
}